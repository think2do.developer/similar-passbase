import React from "react";
import { Container } from "reactstrap";
import Header from "../components/header.js";
import SEO from "../components/seo";

class Layout extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}
  render() {
    return (
      <div className="body-wrapper wrapper">
        <SEO title={this.props.title} />
        <Header isShowMenu={true} isShowButton={true} />

        <div className="min-vh-100 d-flex flex-column align-items-center justify-content-center bg-light">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Layout;
