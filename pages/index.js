import React, { Component } from "react";
import Layout from "../layouts";
import Link from "next/link";

class Home extends Component {
  render() {
    return (
      <Layout>
        <h2 className="font-weight-bolder">Welcome to our demo</h2>

        <Link href="/register">
          <a className="h5 font-weight-normal">
            You don't have an account &rarr;
          </a>
        </Link>
        <Link href="/verify">
          <a className="h5 font-weight-normal">You had an account &rarr;</a>
        </Link>
      </Layout>
    );
  }
}

export default Home;
