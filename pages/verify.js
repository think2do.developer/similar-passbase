import React, { Component } from "react";
import Layout from "../layouts";
import dynamic from "next/dynamic";
const VerifyUser = dynamic(() => import("../components/verifyUser"), {
  ssr: false,
});

class Register extends Component {
  render() {
    return (
      <Layout>
        <VerifyUser />
      </Layout>
    );
  }
}

export default Register;
