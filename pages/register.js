import React, { Component } from "react";
import Layout from "../layouts";
import dynamic from "next/dynamic";
const StoreFaceAndIDCard = dynamic(
  () => import("../components/storeFaceAndIDCard"),
  { ssr: false }
);

class Register extends Component {
  render() {
    return (
      <Layout>
        <div className="text-center">
          <h3>Welcome to our demo</h3>
          <h5 className="font-weight-normal">
            Let register an account to begin this demo
          </h5>
        </div>
        <StoreFaceAndIDCard />
      </Layout>
    );
  }
}

export default Register;
