import React, { Component } from "react";
import Layout from "../layouts";
import Link from "next/link";
import Sidebar from "../components/sidebar";
import UserTable from "../components/dashboard/table";

class Dashboard extends Component {
  render() {
    const users = [
      {
        id: "103736",
        name: "Minh Doan",
        gender: "male",
        email: "minhdoan82@gmail.com",
        dateOfBirth: "02/05/1980",
        address: "Mountain View, CA",
        expiredDate: "09/01/2030",
        faceImage: "https://ik.imagekit.io/gsozk5bngn/cr7_f6lPMo-C6c.jpg",
        fontSideCardImage:
          "https://www.am22tech.com/wp-content/uploads/2020/03/visa-holder-driving-license-renewal-in-usa-coronavirus.jpg",
        backSideCardImage:
          "https://bloximages.chicago2.vip.townnews.com/madison.com/content/tncms/assets/v3/editorial/3/22/322cb36c-ecbb-548c-bd23-20d64349c8f4/523f9aca31956.preview-1024.jpg?resize=1024%2C660",
        score: "30%",
      },
    ];
    return (
      <Layout>
        <div className="w-100 h-100 d-flex">
          <Sidebar />
          <div className="p-4 w-100">
            <UserTable users={users} />
          </div>
        </div>
      </Layout>
    );
  }
}

export default Dashboard;
