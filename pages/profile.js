import React, { Component } from "react";
import Layout from "../layouts";
import dynamic from "next/dynamic";
const StoreFaceAndIDCard = dynamic(
  () => import("../components/storeFaceAndIDCard"),
  { ssr: false }
);

class Register extends Component {
  render() {
    return (
      <Layout>
        <StoreFaceAndIDCard />
      </Layout>
    );
  }
}

export default Register;
