/* eslint-disable prettier/prettier */
import http from "../lib/http";

export const getTenantById = (id) => {
  return http.get("/tenants/" + id).then((res) => {
    return {
      success: true,
      data: res,
    };
  });
};

export const createNewTenant = (data) => {
  return http.post("/tenants", data).then((res) => {
    return { success: true, data: res };
  });
};

export const addFaceImage = (id, data) => {
  // eslint-disable-next-line no-undef
  const formData = new FormData();
  formData.set("body", JSON.stringify({ _id: id }));
  formData.append("data", data);
  // return http.post('/image', formData, {
  //   headers: {
  //     'content-type': 'multipart/form-data'
  //   }
  // })
  return {
    success: true,
    url:
      "https://codelight-data.s3-us-west-1.amazonaws.com/face_demo_data/id0_50.jpg",
  };
};

export const recognizeByInFrontCard = (data) => {
  // eslint-disable-next-line no-undef
  const formData = new FormData();
  formData.set("body", JSON.stringify({ type: "driver-license" }));
  formData.append("data", data);
  // return http.post('/image', formData, {
  //   headers: {
  //     'content-type': 'multipart/form-data'
  //   }
  // })
  return {
    success: true,
    res: {
      DL: "I1234562",
      FN: "ALEXANDER JOSEPH",
      LN: "SAMPLE",
      ADDR: "2570 24TH STREET ANYTOWN, CA 95818",
      confident: 0.4,
      DOB: "08/31/1977",
      EXP: "08/31/2014",
      conclusion: true,
    },
  };
};

export const recognizeByFace = (data) => {
  // eslint-disable-next-line no-undef
  const formData = new FormData();

  formData.append("data", data);
  // return http.post('/image', formData, {
  //   headers: {
  //     'content-type': 'multipart/form-data'
  //   }
  // })
  return {
    success: true,
    res: {
      _id: 434343,
      name: "Messi",
      most_relevant_image: "link",
      similar: 0.4,
      averageSimilar: 0.5706719234585762,
      conclusion: true,
    },
  };
};
