import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  Row,
  Col,
} from "reactstrap";
import Statistc from "./statistic";

const rootState = {};
class UserModal extends Component {
  constructor(props) {
    super(props);
    this.state = rootState;
    this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    this.props.toggle();
  };

  render() {
    const { user } = this.props;
    return (
      <Modal
        size="lg"
        className="modal-dialog-centered"
        isOpen={this.props.show}
        toggle={this.toggle}
      >
        <div className="p-3 text-right">
          <div className="p-2 text-right">
            <img
              href="#"
              onClick={this.toggle}
              height="24"
              width="24"
              src="https://ik.imagekit.io/gsozk5bngn/criss-cross__1__WrpgdTUfZ.svg"
            />
          </div>
        </div>
        <Form className="h-100">
          <ModalBody>
            <Row>
              {/* <Col className="border-right" cols="12" md="2" lg="2"></Col> */}
              <Col cols="12" md="12" lg="12">
                <Statistc user={user} />
              </Col>
            </Row>
          </ModalBody>
        </Form>
        <ModalFooter className="border-0 justify-content-center">
          <Button color="primary">Approve</Button>
          <Button color="secondary" outline>
            Reject
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default UserModal;
