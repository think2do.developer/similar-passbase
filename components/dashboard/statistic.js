import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  Progress,
  Row,
  Col,
} from "reactstrap";

const rootState = {};
class Statistc extends Component {
  constructor(props) {
    super(props);
    this.state = rootState;
    this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    this.props.toggle();
  };

  render() {
    const { user } = this.props;
    const details = [
      {
        title: "Facematch Factor",
        value: 10,
      },
      {
        title: "ID Authenticity",
        value: 20,
      },
      {
        title: "Liveness Detection",
        value: 30,
      },
      {
        title: "Overall Factor",
        value: 40,
      },
    ];
    const confirms = [
      {
        title: "Age over 18",
        active: true,
      },
      {
        title: "ID not expired",
        status: false,
      },
      {
        title: "Valid Document",
        status: false,
      },
      {
        title: "Valid e-mail",
        status: false,
      },
    ];
    return (
      <Row className="w-100 statistc">
        <Col cols="4" md="4" lg="4" className="text-center">
          <h1>{user.score}</h1>
          <h4>Total Score</h4>
        </Col>
        <Col cols="8" md="8" lg="8">
          <Row className="w-100">
            {details.map((item) => {
              return (
                <Col cols="12" md="6" lg="6" key={item.title} className="mb-3">
                  <p>{item.title}</p>
                  <Progress value={item.value} />
                </Col>
              );
            })}
          </Row>
        </Col>
        <Col cols="12" md="12" lg="12">
          <Row className="w-100">
            {confirms.map((item) => {
              return (
                <Col
                  cols="12"
                  md="3"
                  lg="3"
                  key={item.title}
                  className="mb-3 d-flex"
                >
                  <img
                    height="20"
                    width="20"
                    alt=""
                    className="mr-2"
                    src={
                      item.active
                        ? "https://ik.imagekit.io/gsozk5bngn/tick_J4CodeTfX.svg"
                        : "https://ik.imagekit.io/gsozk5bngn/check_lflAv_HGzi.svg"
                    }
                  />
                  <p>{item.title}</p>
                </Col>
              );
            })}
          </Row>
        </Col>
        <Col cols="12" md="12" lg="12">
          <Row className="w-100">
            <Col cols="12" md="6" lg="6" className="mb-3 ">
              <p className="font-weight-bold">Face image</p>
              <img
                alt=""
                className="mr-2 img-fluid img-thumbnail w-100"
                src={user.faceImage}
              />
            </Col>
            <Col cols="12" md="6" lg="6" className="mb-3">
              <p className="font-weight-bold">Identity document</p>

              <img
                alt=""
                className="mr-2 img-fluid img-thumbnail w-100"
                src={user.fontSideCardImage}
              />
              <p className="font-weight-bold">Front</p>

              <img
                alt=""
                className="mr-2 img-fluid img-thumbnail w-100"
                src={user.backSideCardImage}
              />
              <p className="font-weight-bold">Back</p>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Statistc;
