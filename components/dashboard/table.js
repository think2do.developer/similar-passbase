import React from "react";
import { Table, Card, CardBody, Button } from "reactstrap";
import UserModal from "./modal";
class UserTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { show: false };
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({ show: !this.state.show });
  }
  componentDidMount() {}

  render() {
    const { users } = this.props;
    return (
      <Card className="w-100">
        <CardBody>
          <Table striped responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th> Date</th>
                <th> Name</th>
                <th>Email</th>
                <th>Score</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => {
                return (
                  <tr key={user.id}>
                    <UserModal
                      user={user}
                      show={this.state.show}
                      toggle={this.toggle}
                    />
                    <th scope="row">{user.id}</th>
                    <td>July 3, 2020</td>
                    <td>{user.name}</td>
                    <td>{user.email} </td>
                    <td>{user.score}</td>
                    <td>
                      <a href="#" onClick={this.toggle}>
                        Check
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

export default UserTable;
