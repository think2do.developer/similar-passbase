import React from "react";
import Link from "next/link";
import { withRouter } from "next/router";

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    const path = this.props.router.pathname;
    const routes = [
      {
        name: "Dashboard",
        url: "/dashboard",
        icon: "fas fa-tachometer-alt",
      },
    ];

    return (
      <div className="vh-100 side-bar bg-white overflow-hidden d-none d-md-block d-lg-block">
        <div className="dashboard-menu">
          <ul className="pl-0 list-unstyled list-group">
            {routes.map((route, index) => {
              return (
                <Link key={route.name} href={route.url}>
                  <a className="text-muted">
                    <li
                      className={`p-3 list-group-item list-group-item-action rounded-0 ${
                        path.indexOf(route.url) > -1 ? "bg-light" : ""
                      }`}
                    >
                      <i className={`${route.icon} mr-2`}></i>
                      {route.name}
                    </li>
                  </a>
                </Link>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default withRouter(Sidebar);
