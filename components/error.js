import React from "react";

class ErrorStep extends React.Component {
  render() {
    setTimeout(() => {
      this.props.callBack();
    }, 2000);
    return (
      <div className="id-card d-flex flex-column align-items-center">
        <img
          className="user-avatar"
          src="https://ik.imagekit.io/gsozk5bngn/criss-cross__2__ylmDSKDh8.svg"
        />
        <h3 className="text-center mb-3">Something went wrong! </h3>
        <h5>Please try again</h5>
      </div>
    );
  }
}

export default ErrorStep;
