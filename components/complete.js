import React from "react";
import Profile from "./profile";
class CompleteStep extends React.Component {
  render() {
    setTimeout(() => {
      this.props.callBack();
    }, 2000);
    return (
      <div className="id-card d-flex flex-column align-items-center">
        <img
          className="user-avatar"
          src="https://ik.imagekit.io/gsozk5bngn/tick_J4CodeTfX.svg"
        />
        <h3 className="text-center mb-3">Congrats! </h3>
      </div>
    );
  }
}

export default CompleteStep;
