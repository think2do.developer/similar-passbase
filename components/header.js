import React, { Component } from "react";
import { Navbar, NavbarBrand, Container } from "reactstrap";
import Link from "next/link";
class Header extends Component {
  constructor(props) {
    super(props);
    this.scroll = this.scroll.bind(this);
  }
  componentDidMount() {
    window.addEventListener("scroll", this.scroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.scroll);
  }
  scroll() {
    let top = window.scrollY;
    if (top >= 100) {
      this.setState({ navBackground: "bg-light shadow" });
    } else {
      this.setState({ navBackground: "bg-light" });
    }
  }

  render() {
    return (
      <div className="header shadow sticky-top bg-white" onScroll={this.scroll}>
        <Navbar className="" expand="md" className="py-2">
          <Container fluid>
            <Link href="/">
              <a>
                <NavbarBrand className="text-home-page-primary font-weight-bold text-secondary">
                  Demo
                </NavbarBrand>
              </a>
            </Link>
          </Container>
        </Navbar>
      </div>
    );
  }
}

export default Header;
