/* eslint-disable prettier/prettier */
import React from "react";

class Welcome extends React.Component {
  componentDidMount() {}
  render() {
    const {
      title = "Register an account",
      subtitle = "Register by a selfie and picture of your ID.",
    } = this.props;
    return (
      <div className="id-card d-flex flex-column align-items-center">
        <img
          alt=""
          height="300"
          width="300"
          className="img-fluid"
          src="https://ik.imagekit.io/gsozk5bngn/main_start_info_explanation_zJpa_Cxyx.gif"
        />
        <h3 className="text-center mb-3">{title}</h3>
        <h5 className="text-center text-muted mb-4">{subtitle}</h5>
      </div>
    );
  }
}

export default Welcome;
