/* eslint-disable prettier/prettier */
import React from "react";
import { FormGroup } from "reactstrap";

class Prepare extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const value = event.target.checked;
    this.props.handleChange({
      target: {
        name: "check",
        value: value,
      },
    });
  }

  render() {
    return (
      <div className="id-card d-flex flex-column align-items-center">
        <h3 className="text-center mb-4">Please prepare your register</h3>
        <div className="p-3">
          <p className="w-100">
            <span className="list-prepare mr-2">1</span>Agree to terms
          </p>
          <p className="w-100">
            <span className="list-prepare mr-2">2</span>Good light for selfie
            video
          </p>
          <p className="w-100">
            <span className="list-prepare mr-2">3</span>Passport, Driving
            License or National ID Card
          </p>
          <FormGroup>
            <label className="checkbox-container">
              <p className="h6 w-100 mb-3">
                Yes, please start by verifying my identity. I have read the
                <a href="#" target="_blank" rel="noreferrer" className="mx-1">
                  privacy policy
                </a>
                and
                <a href="#" target="_blank" rel="noreferrer" className="mx-1">
                  terms and conditions
                </a>
                and agree to them.
              </p>
              <input
                onChange={this.onChange}
                name="check"
                type="checkbox"
                className="rounded"
              />
              <span className="checkmark rounded" />
            </label>
          </FormGroup>
        </div>
      </div>
    );
  }
}

export default Prepare;
