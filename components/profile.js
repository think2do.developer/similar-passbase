import React from "react";
import { Row, Col, Card, CardBody } from "reactstrap";
class UserProfile extends React.Component {
  componentDidMount() {}
  render() {
    const { user } = this.props;
    return (
      <Card className="w-100">
        <CardBody>
          <Row>
            <Col cols="4" md="4" lg="4">
              <img
                alt=""
                className="img-fluid img-thumbnail w-100"
                src={user.faceImage}
              />
            </Col>
            <Col cols="8" md="8" lg="8">
              <div className="d-flex">
                <p className="font-weight-bold mb-1 mr-2">ID:</p>
                <p className="mb-1">{user.id}</p>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold mb-1 mr-2">Name:</p>
                <p className="mb-1">{user.name}</p>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold mb-1 mr-2">Gender:</p>
                <p className="mb-1">{user.gender}</p>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold mb-1 mr-2">Date of birth:</p>
                <p className="mb-1">{user.dateOfBirth}</p>
              </div>
              <div className="d-flex">
                <p className="font-weight-bold mb-1 mr-2">Address:</p>
                <p className="mb-1">{user.address}</p>
              </div>

              <div className="d-flex ">
                <p className="font-weight-bold mb-1 mr-2">
                  License expired at:
                </p>
                <p className="mb-1">{user.expiredDate}</p>
              </div>
              <div className="d-flex flex-column align-items-start">
                <p className="font-weight-bold">Identity document:</p>
                <img
                  alt=""
                  className="mr-2 img-fluid img-thumbnail w-100"
                  height="200"
                  src={user.fontSideCardImage}
                />
                <p className="font-weight-bold">Front</p>

                <img
                  alt=""
                  className="mr-2 img-fluid img-thumbnail w-100"
                  height="200"
                  src={user.backSideCardImage}
                />
                <p className="font-weight-bold">Back</p>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}

export default UserProfile;
