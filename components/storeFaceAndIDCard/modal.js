/* eslint-disable prettier/prettier */
import React, { Component } from "react";
import { Modal, ModalBody, ModalFooter, Button, Form } from "reactstrap";
import FaceCamera from "./face-camera";
import BackScanIdCard from "./back-id-card-scan";
import FrontScanIdCard from "./front-id-card-scan";
import Info from "./info";
// import { createNewTenant, addFaceImage } from '../../api/api'
// import _get from 'lodash.get'
import Welcome from "../welcome";
import Prepare from "../prepare";
import BackIdCardVideoScan from "./back-id-card-video-scan";
import CompleteStep from "../complete";
import Router from "next/router";
const rootState = {
  step: 0,
  email: "",
  tenant: {},
  face: "",
  check: false,
  frontIdCard: "",
};
class CustomModal extends Component {
  constructor(props) {
    super(props);
    this.state = rootState;
    this.toggle = this.toggle.bind(this);
    this.getComponent = this.getComponent.bind(this);
    this.getButtonComponent = this.getButtonComponent.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.prevStep = this.prevStep.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.createTenant = this.createTenant.bind(this);
    this.addFaceImage = this.addFaceImage.bind(this);
    this.addFrontIdCard = this.addFrontIdCard.bind(this);
    this.handleBackIdCard = this.handleBackIdCard.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  toggle = () => {
    this.props.toggle();
    this.setState({ ...rootState });
  };

  getComponent() {
    const { step } = this.state;
    switch (step) {
      case 1:
        return <Prepare handleChange={this.handleChange} />;
      case 2:
        return <Info handleChange={this.handleChange} />;
      case 3:
        return <FaceCamera handleFile={this.handleFile} />;
      case 4:
        return <FrontScanIdCard handleFile={this.handleFile} />;
      case 5:
        return <BackIdCardVideoScan />;
      case 6:
        return (
          <CompleteStep
            callBack={() => {
              Router.push("/dashboard");
            }}
          />
        );

      default:
        return <Welcome />;
    }
  }

  getButtonComponent(step) {
    const { email, face, check, frontIdCard } = this.state;
    switch (step) {
      case 1:
        return (
          <Button
            size="lg"
            className="px-5"
            disabled={!check}
            onClick={this.nextStep}
            color="primary"
          >
            Continue
          </Button>
        );
      case 2:
        return (
          <Button
            onClick={this.createTenant}
            size="lg"
            disabled={!email}
            className="px-5"
            color="primary"
          >
            Continue
          </Button>
        );
      case 3:
        return (
          <Button
            size="lg"
            className="px-5"
            disabled={!face}
            onClick={this.addFaceImage}
            color="primary"
          >
            Continue
          </Button>
        );
      case 4:
        return (
          <Button
            size="lg"
            className="px-5"
            disabled={!frontIdCard}
            onClick={this.addFrontIdCard}
            color="primary"
          >
            Continue
          </Button>
        );
      case 5:
        return (
          <Button
            size="lg"
            className="px-5"
            onClick={this.handleBackIdCard}
            color="primary"
          >
            Continue
          </Button>
        );
      case 6:
        return "";
      default:
        return (
          <Button
            size="lg"
            className="px-5"
            onClick={this.nextStep}
            color="primary"
          >
            Continue
          </Button>
        );
    }
  }

  nextStep() {
    const { step } = this.state;
    this.setState({ step: step + 1 });
  }

  prevStep() {
    const step = this.state.step - 1;
    this.setState({ step: step === 0 ? 1 : step });
  }

  handleChange(event) {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  }

  handleFile(file) {
    this.setState({
      [file.name]: file.value,
    });
  }

  async createTenant(event) {
    event.preventDefault();
    // const { name, gender, age } = this.state
    // const res = await createNewTenant({ name: name, gender: gender, age: age })
    // if (_get(res, 'success', false)) {
    //   this.setState({ tenant: res.data })
    // }
    this.nextStep();
  }

  async addFaceImage() {
    // const { face, tenant } = this.state
    // const res = await addFaceImage(tenant._id, face)
    // if (_get(res, 'success', false)) {
    //   console.log(res)
    // }
    this.nextStep();
  }

  async addFrontIdCard() {
    // const { frontIdCard, tenant } = this.state
    // const res = await addFaceImage(tenant._id, frontIdCard)
    // if (_get(res, 'success', false)) {
    //   console.log(res)
    // }
    this.nextStep();
  }

  async handleBackIdCard(text) {
    this.nextStep();
    console.log(text);
    // this.props.toggle()
  }

  render() {
    const { step } = this.state;
    return (
      <Modal
        size="lg"
        className="modal-dialog-centered"
        isOpen={this.props.show}
        toggle={() => this.toggle()}
      >
        {step < 6 ? (
          <div className="p-2 text-right">
            <img
              href="#"
              onClick={(event) => {
                event.preventDefault();
                this.toggle();
              }}
              height="24"
              width="24"
              src="https://ik.imagekit.io/gsozk5bngn/criss-cross__1__WrpgdTUfZ.svg"
            />
          </div>
        ) : (
          ""
        )}
        <Form className="h-100">
          <ModalBody className="h-100 d-flex flex-column justify-content-center">
            {this.getComponent()}
          </ModalBody>
        </Form>
        <ModalFooter className="border-0">
          <div className="text-right">{this.getButtonComponent(step)}</div>
        </ModalFooter>
      </Modal>
    );
  }
}

export default CustomModal;
