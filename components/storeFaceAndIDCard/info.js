/* eslint-disable prettier/prettier */
import React from "react";
import { FormGroup, Input, Row, Col } from "reactstrap";

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    this.props.handleChange(event);
  }

  render() {
    const fields = [
      {
        name: "email",
        text: "Email",
        type: "text",
      },
    ];
    return (
      <Row className=" justify-content-center">
        <h2 className="text-center mb-3">Enter your email address</h2>
        <h5 className="text-center text-muted mb-4">
          This way you'll be able to monitor who has access to your data.
        </h5>
        <Col cols="12" md="10" lg="10" className="pt-4">
          {fields.map((field) => {
            return (
              <FormGroup key={field.name}>
                {field.type === "select" ? (
                  <Input
                    name={field.name}
                    type={field.type}
                    placeholder={field.name}
                    onChange={this.onChange}
                    required
                  >
                    {field.type === "select"
                      ? field.options.map((item) => {
                          return (
                            <option key={item.value} value={item.value}>
                              {item.text}
                            </option>
                          );
                        })
                      : ""}
                  </Input>
                ) : (
                  <Input
                    key={field.name}
                    name={field.name}
                    placeholder={field.text}
                    type={field.type}
                    required
                    onChange={this.onChange}
                  />
                )}
              </FormGroup>
            );
          })}
        </Col>
      </Row>
    );
  }
}

export default Info;
