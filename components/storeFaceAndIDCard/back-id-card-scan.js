/* eslint-disable react/jsx-boolean-value */
/* eslint-disable prettier/prettier */
import React from 'react'
import Webcam from 'react-webcam'
import { Button } from 'reactstrap'
// import { dataURLtoFile } from "../../utils/helper.js";
import { handleImageScan } from '../../utils/barcode'

class BackScanIdCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = { img: null, facingMode: 'user' }
    this.cameraRef = React.createRef()
    this.takePhoto = this.takePhoto.bind(this)
    this.scan = this.scan.bind(this)
    this.swicthCamera = this.swicthCamera.bind(this)
  }

  takePhoto() {
    const imageSrc = this.cameraRef.current.getScreenshot()
    this.setState({ img: imageSrc })
  }

  async scan() {
    const result = await handleImageScan()
    this.props.handleBackIdCard(result)
  }

  swicthCamera(event) {
    event.preventDefault()
    const { facingMode } = this.state
    this.setState({
      facingMode: facingMode === 'user' ? 'environment' : 'user'
    })
  }

  render() {
    const { img, facingMode } = this.state
    const videoConstraints = {
      facingMode: { exact: facingMode }
    }
    return (
      <div className='id-card d-flex flex-column align-items-center'>
        <h3 className='text-center mb-3'>Scan back side of your ID card</h3>
        {img ? (
          <div>
            <img id='bar-code-img' alt='' src={img} />
            <div id='pdf417-3'>
              <div className='d-flex py-3'>
                <Button
                  onClick={() => this.scan()}
                  color='primary'
                  className='my-2 mr-2'
                >
                  Decode
                </Button>
                <Button
                  color='primary'
                  style={{ height: 'fit-content' }}
                  className='my-2 mr-2'
                  onClick={() => {
                    this.setState({ img: null })
                  }}
                >
                  <img
                    height='24'
                    width='24'
                    src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
                    alt='...'
                  />
                </Button>
              </div>
              <div>
                <label>Result:</label>
                <blockquote>
                  <pre>
                    <code id='result' />
                  </pre>
                </blockquote>
              </div>
            </div>
          </div>
        ) : (
          <div className='d-flex flex-column align-items-center'>
            <div className='w-100 py-3'>
              <Button
                color='primary'
                onClick={(event) => this.swicthCamera(event)}
                className='d-flex align-items-center'
              >
                <img
                  height='24'
                  width='24'
                  className='mr-2'
                  src='https://ik.imagekit.io/gsozk5bngn/switch-camera_pglJXSZeS.svg'
                  alt='...'
                />
                Switch camera
              </Button>
            </div>
            <Webcam
              className='w-100 mb-2 rounded'
              ref={this.cameraRef}
              videoConstraints={videoConstraints}
              webkit-playsinline='true'
            />
            <Button
              className='d-flex justify-content-center rounded-pill'
              color='primary'
              size='lg'
              onClick={this.takePhoto}
            >
              <img
                height='24'
                width='24'
                src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
                alt='...'
              />
            </Button>
          </div>
        )}
      </div>
    )
  }
}

export default BackScanIdCard
