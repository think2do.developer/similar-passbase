/* eslint-disable prettier/prettier */
import React from 'react'
import Webcam from 'react-webcam'
import { Button } from 'reactstrap'
import { dataURLtoFile } from '../../utils/helper.js'

class FrontIdCardScan extends React.Component {
  constructor(props) {
    super(props)
    this.state = { img: null, facingMode: 'user' }
    this.cameraRef = React.createRef()
    this.swicthCamera = this.swicthCamera.bind(this)
    this.takePhoto = this.takePhoto.bind(this)
  }

  takePhoto() {
    const imageSrc = this.cameraRef.current.getScreenshot()
    const file = { name: 'frontIdCard', value: dataURLtoFile(imageSrc) }
    this.props.handleFile(file)
    this.setState({ img: imageSrc })
  }

  swicthCamera(event) {
    event.preventDefault()
    const { facingMode } = this.state
    this.setState({
      facingMode: facingMode === 'user' ? 'environment' : 'user'
    })
  }

  render() {
    const { img, facingMode } = this.state
    const videoConstraints = {
      facingMode: { exact: facingMode }
    }
    return (
      <div className='id-card d-flex flex-column align-items-center'>
        <h3 className='text-center mb-3'>Get front side of your ID card</h3>

        {img ? (
          <div className='d-flex flex-column align-items-center'>
            <img className='img-thumbnail' alt='' src={img} />
            <Button
              color='primary'
              style={{ height: 'fit-content' }}
              className='my-2 mr-2'
              onClick={() => {
                this.setState({ img: null })
              }}
            >
              <img
                height='24'
                width='24'
                src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
                alt='...'
              />
            </Button>
          </div>
        ) : (
          <div className='d-flex flex-column align-items-center'>
            <div className='w-100 py-3'>
              <Button
                color='primary'
                onClick={(event) => this.swicthCamera(event)}
                className='d-flex align-items-center'
              >
                <img
                  height='24'
                  width='24'
                  className='mr-2'
                  src='https://ik.imagekit.io/gsozk5bngn/switch-camera_pglJXSZeS.svg'
                  alt='...'
                />
                Switch camera
              </Button>
            </div>
            <Webcam
              className='rounded img-fluid'
              videoConstraints={videoConstraints}
              ref={this.cameraRef}
              webkit-playsinline='true'
            />
            <Button
              className='d-flex justify-content-center rounded-pill my-3'
              color='primary'
              size='lg'
              onClick={this.takePhoto}
            >
              <img
                height='24'
                width='24'
                src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
                alt='...'
              />
            </Button>
          </div>
        )}
      </div>
    )
  }
}

export default FrontIdCardScan
