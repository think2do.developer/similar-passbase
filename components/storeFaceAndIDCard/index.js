/* eslint-disable prettier/prettier */
import React from "react";
import { Button } from "reactstrap";
import CustomModal from "./modal";

class StoreFaceAndId extends React.Component {
  constructor(props) {
    super(props);
    this.state = { show: false };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ show: !this.state.show });
  }

  render() {
    const { show } = this.state;
    return (
      <div className="m-2">
        <CustomModal show={show} toggle={this.toggle} />
        <Button
          color="white"
          size="lg"
          className="shadow font-weight-bolder px-4 d-flex align-items-center"
          onClick={this.toggle}
        >
          <img
            height="24"
            width="24"
            className="mr-2"
            src="https://ik.imagekit.io/gsozk5bngn/edit_2dbLpRCHG.svg"
          />
          Register
        </Button>
      </div>
    );
  }
}

export default StoreFaceAndId;
