/* eslint-disable prettier/prettier */
import React from 'react'
import { handleVideoScan } from '../../utils/barcode'

class BackIdCardVideoScan extends React.Component {
  constructor(props) {
    super(props)
    this.state = { img: null, facingMode: 'user' }
    this.cameraRef = React.createRef()
    this.takePhoto = this.takePhoto.bind(this)
    this.scan = this.scan.bind(this)
    this.swicthCamera = this.swicthCamera.bind(this)
  }

  takePhoto() {
    const imageSrc = this.cameraRef.current.getScreenshot()
    this.setState({ img: imageSrc })
  }

  async scan() {
    // const result = await handleVideoScan()
    // this.props.handleBackIdCard(result)
  }

  swicthCamera(event) {
    event.preventDefault()
    const { facingMode } = this.state
    this.setState({
      facingMode: facingMode === 'user' ? 'environment' : 'user'
    })
  }

  async componentDidMount() {
    const res = await handleVideoScan()
    console.log(res)
  }

  render() {
    return (
      <div className='id-card d-flex flex-column align-items-center'>
        <h3 className='text-center mb-3'>Scan back side of your ID card</h3>
        <div className='py-3'>
          <span className='mr-2 btn btn-primary' id='startButton'>
            <img
              height='24'
              width='24'
              src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
              alt='...'
            />
          </span>
          <span className='mr-2 btn btn-primary' id='resetButton'>
            <img
              height='24'
              width='24'
              src='https://ik.imagekit.io/gsozk5bngn/switch-camera_pglJXSZeS.svg'
              alt='...'
            />
          </span>
        </div>
        <div className='id-card'>
          <video id='video' webkit-playsinline='true' className='rounded' />
        </div>
        <div id='sourceSelectPanel'>
          <label htmlFor='sourceSelect'>Seclect camera:</label>
          <select id='sourceSelect' />
        </div>
        <div className='w-100'>
          <label>Result:</label>
          <pre>
            <code className='w-100' id='result' />
          </pre>
        </div>
      </div>
    )
  }
}

export default BackIdCardVideoScan
