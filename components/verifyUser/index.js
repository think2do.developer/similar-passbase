import React from "react";
import { Button } from "reactstrap";
import CustomModal from "./modal";
import UserProfile from "../profile";

class StoreFaceAndId extends React.Component {
  constructor(props) {
    super(props);
    this.state = { show: false, user: null };
    this.toggle = this.toggle.bind(this);
    this.handleResult = this.handleResult.bind(this);
  }

  toggle() {
    this.setState({ show: !this.state.show });
  }
  handleResult(user) {
    this.setState({
      user: user,
    });
  }
  render() {
    const { show, user } = this.state;
    return (
      <div className="m-2">
        {user ? (
          <div className="text-center">
            <h3>Your profile</h3>
            <UserProfile user={user} />
          </div>
        ) : (
          <div className="text-center">
            <h3>Welcome to our demo</h3>
            <h5 className="font-weight-normal">
              Let verify your account to continue this demo
            </h5>

            <CustomModal
              show={show}
              toggle={this.toggle}
              handleResult={this.handleResult}
            />
            <Button
              color="white"
              size="lg"
              className="shadow font-weight-bold px-4"
              onClick={this.toggle}
            >
              <img
                height="24"
                width="24"
                className="mr-3"
                src="https://ik.imagekit.io/gsozk5bngn/shield_XAFa5gt-b.svg"
              />
              Verify
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default StoreFaceAndId;
