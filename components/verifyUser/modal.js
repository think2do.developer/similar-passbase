import React, { Component } from "react";
import { Modal, ModalBody, ModalFooter, Button, Form } from "reactstrap";
import FaceCamera from "./face-camera";
import Info from "./info";
// import { createNewTenant, addFaceImage } from '../../api/api'
import _get from "lodash.get";
import Welcome from "../welcome";
import CompleteStep from "../complete";
import { getTenantById } from "../../api/api";
import ErrorStep from "../error";
const rootState = {
  step: 0,
  email: "",
  tenant: {},
  face: "",
  check: false,
  frontIdCard: "",
};
class CustomModal extends Component {
  constructor(props) {
    super(props);
    this.state = rootState;
    this.toggle = this.toggle.bind(this);
    this.getComponent = this.getComponent.bind(this);
    this.getButtonComponent = this.getButtonComponent.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.prevStep = this.prevStep.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.addFaceImage = this.addFaceImage.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.verifyUser = this.verifyUser.bind(this);
  }

  toggle = () => {
    this.props.toggle();
    this.setState({ ...rootState });
  };

  handleChange(event) {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  }

  async verifyUser() {
    const { email, face } = this.state;
    const { handleResult } = this.props;
    // const res = await getTenantById("103736");
    // if (_get(res, "success", false)) {
    //   this.toggle();
    //   handleResult(res.data);
    // } else {
    //   this.nextStep();
    // } {
    handleResult({
      id: "103736",
      name: "Minh Doan",
      gender: "male",
      email: "minhdoan82@gmail.com",
      dateOfBirth: "02/05/1980",
      address: "Mountain View, CA",
      expiredDate: "09/01/2030",
      faceImage: "https://ik.imagekit.io/gsozk5bngn/cr7_f6lPMo-C6c.jpg",
      fontSideCardImage:
        "https://www.am22tech.com/wp-content/uploads/2020/03/visa-holder-driving-license-renewal-in-usa-coronavirus.jpg",
      backSideCardImage:
        "https://bloximages.chicago2.vip.townnews.com/madison.com/content/tncms/assets/v3/editorial/3/22/322cb36c-ecbb-548c-bd23-20d64349c8f4/523f9aca31956.preview-1024.jpg?resize=1024%2C660",
      score: "30%",
    });
  }

  getComponent() {
    const { step } = this.state;
    switch (step) {
      case 1:
        return <Info handleChange={this.handleChange} />;
      case 2:
        return <FaceCamera handleFile={this.handleFile} />;
      case 3:
        return <CompleteStep callBack={this.verifyUser} />;
      case 4:
        return (
          <ErrorStep
            callBack={() => {
              this.toggle();
            }}
          />
        );
      default:
        return (
          <Welcome
            title="Start your verification"
            subtitle="Confirm your identity with a selfie and your email."
          />
        );
    }
  }

  getButtonComponent(step) {
    const { email, face } = this.state;
    switch (step) {
      case 1:
        return (
          <Button
            size="lg"
            className="px-5"
            disabled={!email}
            onClick={this.nextStep}
            color="primary"
          >
            Continue
          </Button>
        );

      case 2:
        return (
          <Button
            size="lg"
            className="px-5"
            disabled={!face}
            onClick={this.addFaceImage}
            color="primary"
          >
            Continue
          </Button>
        );
      case 3:
        return "";
      default:
        return (
          <Button
            size="lg"
            className="px-5"
            onClick={this.nextStep}
            color="primary"
          >
            Continue
          </Button>
        );
    }
  }

  nextStep() {
    const { step } = this.state;
    this.setState({ step: step + 1 });
  }

  prevStep() {
    const step = this.state.step - 1;
    this.setState({ step: step === 0 ? 1 : step });
  }

  handleFile(file) {
    this.setState({
      [file.name]: file.value,
    });
  }

  async addFaceImage() {
    // const { face, tenant } = this.state
    // const res = await addFaceImage(tenant._id, face)
    // if (_get(res, 'success', false)) {
    //   console.log(res)
    // }
    this.nextStep();
  }

  render() {
    const { step } = this.state;
    return (
      <Modal
        size="lg"
        className="modal-dialog-centered"
        isOpen={this.props.show}
        toggle={() => this.toggle()}
      >
        {step < 3 ? (
          <div className="p-3 text-right">
            <div className="p-2 text-right">
              <img
                href="#"
                onClick={(event) => {
                  event.preventDefault();
                  this.toggle();
                }}
                height="24"
                width="24"
                src="https://ik.imagekit.io/gsozk5bngn/criss-cross__1__WrpgdTUfZ.svg"
              />
            </div>
          </div>
        ) : (
          ""
        )}
        <Form className="h-100">
          <ModalBody className="h-100 d-flex flex-column justify-content-center">
            {this.getComponent()}
          </ModalBody>
        </Form>
        <ModalFooter className="border-0">
          <div className="text-right">{this.getButtonComponent(step)}</div>
        </ModalFooter>
      </Modal>
    );
  }
}

export default CustomModal;
