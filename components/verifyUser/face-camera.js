/* eslint-disable react/jsx-boolean-value */
/* eslint-disable prettier/prettier */
import React from 'react'
import Webcam from 'react-webcam'
import { Button } from 'reactstrap'
import { dataURLtoFile } from '../../utils/helper.js'

class Camera extends React.Component {
  constructor(props) {
    super(props)
    this.state = { img: null }
    this.cameraRef = React.createRef()
    this.takePhoto = this.takePhoto.bind(this)
  }

  takePhoto() {
    const imageSrc = this.cameraRef.current.getScreenshot()
    const file = { name: 'face', value: dataURLtoFile(imageSrc) }
    this.setState({ img: imageSrc })
    this.props.handleFile(file)
  }

  render() {
    const { img } = this.state
    return (
      <div className='w-100 face-camera d-flex flex-column align-items-center'>
        <h3 className='text-center mb-3'>Get your selfie photo</h3>
        <h5 className='text-center text-muted mb-4'>
          Frame your face in the oval
        </h5>
        {img ? (
          <div className='d-flex flex-column align-items-center'>
            <img className='face-preview' alt='' src={img} />
            <Button
              color='primary'
              style={{ height: 'fit-content' }}
              className='my-2 mr-2'
              onClick={() => {
                this.setState({ img: null })
              }}
            >
              <img
                height='24'
                width='24'
                src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
                alt='...'
              />
            </Button>
          </div>
        ) : (
          <div className='d-flex flex-column align-items-center'>
            <Webcam
              className='bg-transparent'
              webkit-playsinline='true'
              ref={this.cameraRef}
            />
            <Button className='my-2' onClick={this.takePhoto}>
              <img
                height='24'
                width='24'
                src='https://ik.imagekit.io/gsozk5bngn/camera_vOlCUEeq_.svg'
                alt='...'
              />
            </Button>
          </div>
        )}
      </div>
    )
  }
}

export default Camera
