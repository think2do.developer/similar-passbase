/* eslint-disable prettier/prettier */
import { BrowserPDF417Reader, BrowserMultiFormatReader } from '@zxing/library'
export const handleVideoScan = () => {
  let selectedDeviceId
  const codeReader = new BrowserMultiFormatReader()
  codeReader
    .getVideoInputDevices()
    .then((videoInputDevices) => {
      const sourceSelect = document.getElementById('sourceSelect')
      selectedDeviceId = videoInputDevices[0].deviceId
      if (videoInputDevices.length >= 1) {
        videoInputDevices.forEach((element) => {
          const sourceOption = document.createElement('option')
          sourceOption.text = element.label
          sourceOption.value = element.deviceId
          sourceSelect.appendChild(sourceOption)
        })

        sourceSelect.onchange = () => {
          selectedDeviceId = sourceSelect.value
        }
        const sourceSelectPanel = document.getElementById('sourceSelectPanel')
        sourceSelectPanel.style.display = 'block'
      }

      document.getElementById('startButton').addEventListener('click', () => {
        console.log(
          `Started continous decode from camera with id ${selectedDeviceId}`
        )
        return codeReader
          .decodeOnceFromVideoDevice(selectedDeviceId, 'video')
          .then((result) => {
            console.log('Detected barcode!')
            document.getElementById('result').textContent = result.text
            return {
              success: true,
              data: result.text
            }
          })
          .catch((err) => {
            console.log('Detected failed', err)
            document.getElementById('result').textContent = err
            return {
              success: false,
              err: err
            }
          })
      })

      document.getElementById('resetButton').addEventListener('click', () => {
        codeReader.reset()
        document.getElementById('result').textContent = ''
        console.log('Reset.')
      })
    })
    .catch((err) => {
      console.error(err)
    })
}

export const handleImageScan = () => {
  const codeReader = new BrowserPDF417Reader()
  console.log('ZXing code reader initialized')

  const img = document.getElementById('bar-code-img')
  img.crossOrigin = 'Anonymous'
  return codeReader
    .decodeFromImage(img)
    .then((result) => {
      console.log('handleImageScan success', result)
      document.getElementById('result').textContent = result.text
      return result
    })
    .catch((err) => {
      console.log('handleImageScan err', err)
      document.getElementById('result').textContent = err
      return ''
    })
}
