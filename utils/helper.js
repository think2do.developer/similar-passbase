import { v1 as uuid } from "uuid";

export const dataURLtoFile = (dataurl) => {
  var arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], uuid(), { type: mime });
};

export const getMostLikeTenant = (tenants) => {
  const sortedTenants = tenants.sort((a, b) => {
    return b.similar - a.similar;
  });
  return sortedTenants[0];
};
