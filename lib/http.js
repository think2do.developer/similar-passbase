/* eslint-disable prettier/prettier */
import axios from 'axios'

const http = axios.create({
  baseURL: 'http://localhost:3001',
  headers: {
    'Content-type': 'application/json;charset=UTF-8'
  }
})

http.interceptors.response.use(
  function (response) {
    return response.data
  },
  function (error) {
    return Promise.reject(error)
  }
)

export default http
